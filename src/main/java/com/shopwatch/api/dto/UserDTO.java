package com.shopwatch.api.dto;

import lombok.Data;

@Data
public class UserDTO {
	private String username;
	private String password;
	private String email;
}
